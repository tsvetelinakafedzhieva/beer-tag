# BEER-TAG APPLICATION  
**Java Project - *Team 10***

### Project Description  
**Beer-Tag** is web application which enables you to manage all
the beers that you have drank and want to drink. 
Each beer has detailed information about it - from ABV (alcohol by volume) 
to the style and description. Data is community driven and every beer
lover can add new beers. **Beer-Tag** allows you to rate a beer and 
see average rating from different users.  

### Functional Description
Each user has name, email and profile picture.  
Each beer has name, description, origin country, brewery that produces it, style (pre-defined), ABV,
zero, one or many tags and a picture.  

Application provides functionality based on the role of the user that is using it. There are three types of users: **anonymous**, **registered** and **administrator**.  
- **Anonymous users** can browse all beers and their details. They can also filter by origin country, style,
tags and sort by name, ABV and rating.  
- **Registered users** can create new beers, add a beer to their wish list and drank list and rate beers.
They can also edit and delete their own beers. Registered users can modify their personal
information as well.  
**Registered users** also have user profile page that shows 
user’s photo, user personal details, their wish list, drank list and their top 3 most 
ranked beers, they’ve tasted.  
- **Administrators** have administrative access to the system and permissions to execute 
operations as edit/delete of beers and/or users.  

### Technical Description  
***Database:***  
* The data of the application is stored in a relational database – MySQL/MariaDB.    

***Backend:***  
* JDK version 11
* Used tiered project structure (separate the application components in layers) 
* Used SpringMVC and SpringBoot framework 
* Used Hibernate in the persistence (repository) layer 
* Used Spring Security to handle user registration and user roles  

***Frontend:***  
* Used Spring MVC Framework with Thymeleaf template engine for generating the UI  
* Used AJAX for making asynchronous requests to the server where you find it appropriate  
* Applied own design and visual styles  
* Used Bootstrap and Materialize  


***API Documentation:***

- **Link to Swagger:** localhost:8080/swagger-ui.html  

***Deliverables:***  

- **Link to the Trello Board:**  https://trello.com/b/VswbKugU  
