drop database if exists beer_tag;
create database beer_tag;

use beer_tag;


create or replace table breweries
(
    brewery_id     int auto_increment
        primary key,
    name           varchar(60) not null,
    origin_country varchar(50) not null,
    status         varchar(30) not null default 'ACTIVE'
);

create or replace table users
(
    username varchar(50) NOT NULL,
    password varchar(68) NOT NULL,
    enabled  tinyint(4)  NOT NULL,
    PRIMARY KEY (`username`)
);

create or replace table authorities
(
    auth_id   int auto_increment,
    username  varchar(50) NOT NULL,
    authority varchar(50) NOT NULL,
    UNIQUE KEY username_authority (auth_id, username, authority)
);

create or replace table styles
(
    style_id   int auto_increment
        primary key,
    style_name varchar(40) not null,
    is_enable  tinyint(20) not null,
    constraint styles_style_name_uindex
        unique (style_name)
);

create or replace table tags
(
    tag_id int auto_increment
        primary key,
    name   varchar(50) not null
);

create or replace table users_details
(
    user_id     int auto_increment
        primary key,
    first_name  varchar(20)   not null,
    last_name   varchar(20)   not null,
    email       varchar(50)   not null,
    picture_url varchar(2090) not null,
    is_active   tinyint(10)   not null,
    constraint users_email_uindex
        unique (email)
);

create or replace table beers
(
    beer_id          int auto_increment
        primary key,
    name             varchar(50)   not null,
    description      varchar(1000) not null,
    abv_percent      decimal(4, 2) not null,
    style_id         int           not null,
    brewery_id       int           not null,
    owner_id         int           not null,
    beer_picture_url varchar(2090) not null,
    status           varchar(30)   not null default 'ACTIVE',
    constraint beers_ibfk_1
        foreign key (brewery_id) references breweries (brewery_id),
    constraint beers_style_id_fk
        foreign key (style_id) references styles (style_id),
    constraint beers_users_id_fk
        foreign key (owner_id) references users_details (user_id)
);

create or replace index brewery_id
    on beers (brewery_id);

create or replace table beers_tags
(
    beer_id int null,
    tag_id  int null,
    constraint beers_tags_ibfk_1
        foreign key (beer_id) references beers (beer_id),
    constraint beers_tags_ibfk_2
        foreign key (tag_id) references tags (tag_id)
);

create or replace index beer_id
    on beers_tags (beer_id);

create or replace index tag_id
    on beers_tags (tag_id);

create or replace table user_ratings
(
    rating_id int auto_increment
        primary key,
    user_id   int    not null,
    rating    int(5) not null,
    constraint user_ratings_users_id_fk
        foreign key (user_id) references users_details (user_id)
);

create or replace table beer_user_rating
(
    rating_id int null,
    beer_id   int null,
    constraint beer_user_rating_fk
        foreign key (rating_id) references user_ratings (rating_id),
    constraint beer_user_rating_fk2
        foreign key (beer_id) references beers (beer_id)
);

create or replace table users_drank_list
(
    user_id int null,
    beer_id int null,
    constraint users_drank_list_ibfk_1
        foreign key (user_id) references users_details (user_id),
    constraint users_drank_list_ibfk_2
        foreign key (beer_id) references beers (beer_id)
);

create or replace index beer_id
    on users_drank_list (beer_id);

create or replace index user_id
    on users_drank_list (user_id);

create or replace table users_roles
(
    user_id int null,
    auth_id int null,
    constraint users_roles_users_user_id_fk
        foreign key (user_id) references users_details (user_id)
);

create or replace table users_wish_list
(
    user_id int null,
    beer_id int null,
    constraint users_wish_list_ibfk_1
        foreign key (user_id) references users_details (user_id),
    constraint users_wish_list_ibfk_2
        foreign key (beer_id) references beers (beer_id)
);

create or replace index beer_id
    on users_wish_list (beer_id);

create or replace index user_id
    on users_wish_list (user_id);