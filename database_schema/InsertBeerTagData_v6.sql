INSERT INTO users (username, password, enabled) VALUES ('username.ivan@example.com', '{noop}pass1', 1);
INSERT INTO users (username, password, enabled) VALUES ('username.maria@example.com', '{noop}pass1', 1);
INSERT INTO users (username, password, enabled) VALUES ('username.hristo@example.com', '{noop}pass1', 1);
INSERT INTO users (username, password, enabled) VALUES ('username.lora@example.com', '{noop}pass1', 1);
INSERT INTO users (username, password, enabled) VALUES ('username.georgi@example.com', '{noop}pass1', 1);


INSERT INTO beer_tag.authorities (auth_id, username, authority) VALUES (1, 'username.ivan@example.com', 'ROLE_ADMINISTRATOR');
INSERT INTO beer_tag.authorities (auth_id, username, authority) VALUES (2, 'username.maria@example.com', 'ROLE_REGISTERED');
INSERT INTO beer_tag.authorities (auth_id, username, authority) VALUES (3, 'username.hristo@example.com', 'ROLE_REGISTERED');
INSERT INTO beer_tag.authorities (auth_id, username, authority) VALUES (4, 'username.lora@example.com', 'ROLE_REGISTERED');
INSERT INTO beer_tag.authorities (auth_id, username, authority) VALUES (5, 'username.georgi@example.com', 'ROLE_ADMINISTRATOR');
INSERT INTO beer_tag.authorities (auth_id, username, authority) VALUES (6, 'username.georgi@example.com', 'ROLE_REGISTERED');
INSERT INTO beer_tag.authorities (auth_id, username, authority) VALUES (7, 'username.ivan@example.com', 'ROLE_REGISTERED');


INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (1, 'Amber ale', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (2, 'IPA', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (3, 'Pale Ale', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (4, 'Porter', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (5, 'Stout', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (6, 'Witbier', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (7, 'Blonde Ale', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (8, 'Pilsner', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (9, 'Weissbier', 1);
INSERT INTO beer_tag.styles (style_id, style_name, is_enable) VALUES (10, 'Lager', 1);

INSERT INTO beer_tag.tags (tag_id, name) VALUES (1, 'Light Yellow');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (2, 'Straw');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (3, 'Pale');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (4, 'Gold');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (5, 'Light Amber');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (6, 'Amber');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (7, 'Medium Amber');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (8, 'Copper');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (9, 'Light Brown');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (10, 'Saddle Brown');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (11, 'Brown');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (12, 'Dark Brown');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (13, 'Black');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (14, 'Coffee');
INSERT INTO beer_tag.tags (tag_id, name) VALUES (15, 'Chocolate');

INSERT INTO beer_tag.users_details (user_id, first_name, last_name, email, picture_url, is_active) VALUES (1, 'Ivan', 'Ivanov', 'username.ivan@example.com', 'https://www.ibts.org/wp-content/uploads/2017/08/iStock-476085198.jpg', 1);
INSERT INTO beer_tag.users_details (user_id, first_name, last_name, email, picture_url, is_active) VALUES (2, 'Maria', 'Georgieva', 'username.maria@example.com', 'https://www.ibts.org/wp-content/uploads/2017/08/iStock-476085198.jpg', 1);
INSERT INTO beer_tag.users_details (user_id, first_name, last_name, email, picture_url, is_active) VALUES (3, 'Hristo', 'Hristov', 'username.hristo@example.com', 'https://www.ibts.org/wp-content/uploads/2017/08/iStock-476085198.jpg', 1);
INSERT INTO beer_tag.users_details (user_id, first_name, last_name, email, picture_url, is_active) VALUES (4, 'Lora', 'Dimitrova', 'username.lora@example.com', 'https://www.ibts.org/wp-content/uploads/2017/08/iStock-476085198.jpg', 1);
INSERT INTO beer_tag.users_details (user_id, first_name, last_name, email, picture_url, is_active) VALUES (5, 'Georgi', 'Petrov', 'username.georgi@example.com', 'https://www.ibts.org/wp-content/uploads/2017/08/iStock-476085198.jpg', 1);

INSERT INTO beer_tag.users_roles (user_id, auth_id) VALUES (1, 1);
INSERT INTO beer_tag.users_roles (user_id, auth_id) VALUES (2, 2);
INSERT INTO beer_tag.users_roles (user_id, auth_id) VALUES (3, 3);
INSERT INTO beer_tag.users_roles (user_id, auth_id) VALUES (4, 4);
INSERT INTO beer_tag.users_roles (user_id, auth_id) VALUES (5, 5);
INSERT INTO beer_tag.users_roles (user_id, auth_id) VALUES (5, 6);
INSERT INTO beer_tag.users_roles (user_id, auth_id) VALUES (1, 7);

INSERT INTO beer_tag.breweries (brewery_id, name, origin_country) VALUES (1, 'Bawden Street Brewing Co.', 'USA');
INSERT INTO beer_tag.breweries (brewery_id, name, origin_country) VALUES (2, 'Valor Brewpub', 'USA');
INSERT INTO beer_tag.breweries (brewery_id, name, origin_country) VALUES (3, 'New Belgium Brewing Co.', 'Belgium ');
INSERT INTO beer_tag.breweries (brewery_id, name, origin_country) VALUES (4, 'Zagovor Brewery', 'Russia');
INSERT INTO beer_tag.breweries (brewery_id, name, origin_country) VALUES (5, 'Northern Monk', 'England');
INSERT INTO beer_tag.breweries (brewery_id, name, origin_country) VALUES (6, 'Heineken International', 'Netherlands');
INSERT INTO beer_tag.breweries (brewery_id, name, origin_country) VALUES (7, 'Corona Brewery', 'Mexico');
INSERT INTO beer_tag.breweries (brewery_id, name, origin_country) VALUES (8, 'Zagorka', 'Bulgaria');

INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (1, 'Privates Pale Ale', 'Pacific Northwest-style APA — hop forward ale with Cascade and Amarillo hops balanced by a light malt background with notes of citrus and caramel.', 5.50, 3, 2, 1, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (2, 'Imperial Porter', 'Chocolate malt with semi sweet overtone.', 8.00, 4, 1, 1, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (3, 'Sunshine Wheat', 'Sunshine Wheat combines the refreshing flavors of a Belgian-style witbier with the easy drinkability of an American wheat. This sun-hued ale blends soft', 4.80, 6, 3, 2, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (4, 'Trinity In Black', 'Imperial Stout with tonka beans', 12.50, 5, 4, 2, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (5, 'Northern Star', 'Brewed in collaboration with North Star Coffee Roasters. Smooth', 5.20, 4, 5, 3, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (6, 'Heathen', 'A bold yet balanced New England IPA. Soft', 7.20, 2, 5, 4, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (7, 'Fat Tire', 'Fat Tire Amber is the easy-drinking Amber Ale born in Colorado from New Belgium Brewing Company', 5.20, 1, 3, 4, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (8, 'Heineken', 'Bright and deep golden in color with a balanced hop aroma.', 5.00, 10, 6, 5, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (9, 'Corona Extra', 'Smooth taste balanced between heavier European imports and lighter beer.', 4.50, 10, 7, 5, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');
INSERT INTO beer_tag.beers (beer_id, name, description, abv_percent, style_id, brewery_id, owner_id, beer_picture_url) VALUES (10, 'Zagorka Special', 'Sparkling golden color medium carbonation and thick creamy foam.', 5.00, 10, 8, 5, 'https://cdn0.iconfinder.com/data/icons/beer-set-filled-color/300/205339134Untitled-3-512.png');

INSERT INTO beer_tag.beers_tags (beer_id, tag_id) VALUES (1, 3);
INSERT INTO beer_tag.beers_tags (beer_id, tag_id) VALUES (2, 12);
INSERT INTO beer_tag.beers_tags (beer_id, tag_id) VALUES (3, 3);
INSERT INTO beer_tag.beers_tags (beer_id, tag_id) VALUES (4, 13);
INSERT INTO beer_tag.beers_tags (beer_id, tag_id) VALUES (5, 12);
INSERT INTO beer_tag.beers_tags (beer_id, tag_id) VALUES (6, 3);
INSERT INTO beer_tag.beers_tags (beer_id, tag_id) VALUES (7, 4);

INSERT INTO beer_tag.users_drank_list (user_id, beer_id) VALUES (3, 1);
INSERT INTO beer_tag.users_drank_list (user_id, beer_id) VALUES (3, 6);
INSERT INTO beer_tag.users_drank_list (user_id, beer_id) VALUES (4, 2);

INSERT INTO beer_tag.users_wish_list (user_id, beer_id) VALUES (4, 2);
INSERT INTO beer_tag.users_wish_list (user_id, beer_id) VALUES (2, 7);
INSERT INTO beer_tag.users_wish_list (user_id, beer_id) VALUES (2, 1);
INSERT INTO beer_tag.users_wish_list (user_id, beer_id) VALUES (3, 1);
INSERT INTO beer_tag.users_wish_list (user_id, beer_id) VALUES (3, 6);