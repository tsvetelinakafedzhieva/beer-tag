package com.telerikacademy.team10.beer_tag.repositories;

import com.telerikacademy.team10.beer_tag.models.*;
import com.telerikacademy.team10.beer_tag.models.enums.FilterType;
import com.telerikacademy.team10.beer_tag.models.enums.SortType;
import com.telerikacademy.team10.beer_tag.models.enums.StatusType;
import com.telerikacademy.team10.beer_tag.repositories.contracts.BeerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class BeerRepositoryImpl implements BeerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getBeers(FilterType filterType, String filter, SortType sortType) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> criteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> beerRoot = criteriaQuery.from(Beer.class);
            criteriaQuery = criteriaQuery.select(beerRoot);

            List<Predicate> wherePredicates = new ArrayList<>();
            wherePredicates.add(criteriaBuilder.equal(beerRoot.get("status"), StatusType.ACTIVE));

            switch (filterType) {
                case COUNTRY:
                    Path<Brewery> breweryPath = beerRoot.get("brewery");
                    wherePredicates.add(criteriaBuilder.like(breweryPath.get("originCountry"), like(filter)));
                    break;
                case STYLE:
                    Path<Style> stylePath = beerRoot.get("style");
                    wherePredicates.add(criteriaBuilder.like(stylePath.get("name"), like(filter)));
                    break;
                case TAG:
                    Join<Beer, Tag> tagJoin = beerRoot.join("tags", JoinType.INNER);
                    wherePredicates.add(criteriaBuilder.like(tagJoin.get("name"), like(filter)));
                    break;
                case ANY:
                default:
                    break;
            }

            criteriaQuery = criteriaQuery.where(wherePredicates.toArray(Predicate[]::new));

            switch (sortType) {
                case NAME:
                    criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.asc(beerRoot.get("name")));
                    break;
                case ABV:
                    criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.asc(beerRoot.get("abv")));
                    break;
                case RATING:
                    criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.asc(beerRoot.get("averageRating")));
                    break;
                case ANY:
                default:
                    break;
            }

            return session.createQuery(criteriaQuery).getResultList();
        }
    }

    @Override
    public Optional<Beer> getBeerById(long id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            return Optional.ofNullable(beer);
        }
    }

    @Override
    public Optional<Beer> getBeerByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> criteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> root = criteriaQuery.from(Beer.class);
            criteriaQuery = criteriaQuery.select(root);
            List<Predicate> wherePredicates = new ArrayList<>();

            wherePredicates.add(criteriaBuilder.equal(root.get("name"), name));
            wherePredicates.add(criteriaBuilder.equal(root.get("status"), StatusType.ACTIVE));
            criteriaQuery = criteriaQuery.where(wherePredicates.toArray((Predicate[]::new)));
            Query<Beer> query = session.createQuery(criteriaQuery);


            return query.uniqueResultOptional();
        }
    }

    @Override
    public void createBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.save(beer);
                tx.commit();
            } catch (RuntimeException e) {
                if (tx != null) tx.rollback();
            }
        }
    }

    @Override
    public void updateBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.update(beer);
                tx.commit();
            } catch (RuntimeException e) {
                if (tx != null) tx.rollback();
            }
        }
    }

    @Override
    public void deleteBeer(long id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                Beer beer = session.get(Beer.class, id);
                beer.setStatus(StatusType.INACTIVE);
                session.update(beer);
                tx.commit();
            } catch (RuntimeException e) {
                if (tx != null) tx.rollback();
            }
        }
    }

    @Override
    public Optional<Rating> getBeerRating(long userId, long beerId) {
        try (Session session = sessionFactory.openSession()) {

            NativeQuery<Integer> query = session.createNativeQuery("select user_ratings.rating_id\n" +
                    "from user_ratings\n" +
                    "        join beer_user_rating bur on user_ratings.rating_id = bur.rating_id\n" +
                    "        join beers b on bur.beer_id = b.beer_id\n" +
                    "where user_id = :userId\n" +
                    "and b.beer_id = :beerId");

            query.setParameter("userId", userId);
            query.setParameter("beerId", beerId);
            Optional<Integer> ratingId = query.uniqueResultOptional();
            if (ratingId.isEmpty()) {
                return Optional.empty();
            } else {
                Rating rating = session.get(Rating.class, ratingId.get().longValue());
                return Optional.ofNullable(rating);
            }
        }
    }


    private String like(String condition) {
        return String.format("%%%s%%", condition);
    }
}
