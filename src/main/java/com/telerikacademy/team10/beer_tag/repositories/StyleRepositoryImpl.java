package com.telerikacademy.team10.beer_tag.repositories;

import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.models.Style;
import com.telerikacademy.team10.beer_tag.repositories.contracts.StyleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class StyleRepositoryImpl implements StyleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAllStyles() {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style " +
                    "where enable =:enable", Style.class);
            query.setParameter("enable", true);
            return query.list();
        }
    }

    @Override
    public Style getStyleById(long id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if ((style == null) || (!style.getEnable())) {
                throw new EntityNotFoundException(
                        String.format("Style with ID %d not found!", id));
            }
            return style;
        }
    }

    @Override
    public Optional<Style> getStyleByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style " +
                    "where name = :name and enable =:enable", Style.class);
            query.setParameter("name", name);
            query.setParameter("enable", true);
            List<Style> style = query.list();
            if (style.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format("Style with name %s not found!", name));
            }
            return style.stream().findFirst();
        }
    }

    @Override
    public void create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.save(style);
        }
    }

    @Override
    public void update(Style style) {
        try (Session session = sessionFactory.openSession()) {
            if ((style == null) || (!style.getEnable())) {
                throw new EntityNotFoundException("Style not found!");
            }
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(long id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if ((style == null) || (!style.getEnable())) {
                throw new EntityNotFoundException(
                        String.format("Style with ID %d not found!", id));
            }
            session.beginTransaction();
            style.setEnable(false);
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Style> filterByName(Optional<String> name) {
        if (name.isEmpty()) {
            return getAllStyles();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style " +
                    "where name = :name and enable =:enable", Style.class);
            query.setParameter("name", name.get());
            query.setParameter("enable", true);
            return query.list();
        }
    }

    @Override
    public boolean existsByName(String name) {
        return !filterByName(Optional.ofNullable(name)).isEmpty();
    }
}
