package com.telerikacademy.team10.beer_tag.repositories.contracts;

import com.telerikacademy.team10.beer_tag.models.Tag;

import java.util.Optional;

public interface TagRepository {

    Optional<Tag> getByName(String name);

    void create(Tag tag);
}
