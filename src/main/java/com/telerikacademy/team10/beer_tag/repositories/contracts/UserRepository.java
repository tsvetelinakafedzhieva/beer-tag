package com.telerikacademy.team10.beer_tag.repositories.contracts;

import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.Role;
import com.telerikacademy.team10.beer_tag.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    List<User> getAllUsers();

    User getUserById(long id);

    User getByEmail(String email);

    void create(User user);

    void update(User user);

    void delete(long id);

    List<User> filterByEmail(Optional<String> email);

    boolean existByEmail(String email);

    Role getRole(int roleId);

    List<Beer> top3MostRankedUserBeers(long userId);
}
