package com.telerikacademy.team10.beer_tag.repositories;

import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.Rating;
import com.telerikacademy.team10.beer_tag.models.Role;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.models.enums.StatusType;
import com.telerikacademy.team10.beer_tag.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserSqlRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserSqlRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where active =:active", User.class);
            query.setParameter("active", true);
            return query.list();
        }
    }

    @Override
    public User getUserById(long id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if ((user == null) || (!user.getActive())) {
                throw new EntityNotFoundException(
                        String.format("User with ID %d not found!", id));
            }
            return user;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User " +
                    "where email = :email and active =:active", User.class);
            query.setParameter("email", email);
            query.setParameter("active", true);
            List<User> users = query.list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format("User with email %s not found!", email));
            }
            return users.get(0);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.save(user);
                tx.commit();
            } catch (RuntimeException e) {
                if (tx != null) tx.rollback();
            }
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            if (user == null || (!user.getActive())) {
                throw new EntityNotFoundException("User not found!");
            }
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(long id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null || (!user.getActive())) {
                throw new EntityNotFoundException(
                        String.format("User with ID %d not found!", id));
            }
            session.beginTransaction();
            user.setActive(false);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> filterByEmail(Optional<String> email) {
        if (email.isEmpty()) {
            return getAllUsers();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User " +
                    "where email = :email", User.class);
            query.setParameter("email", email.get());
            return query.list();
        }
    }

    @Override
    public boolean existByEmail(String email) {
        return !filterByEmail(Optional.ofNullable(email)).isEmpty();
    }

    @Override
    public Role getRole(int roleId) {
        try (Session session = sessionFactory.openSession()) {
            Role role = session.get(Role.class, roleId);
            if (role == null) {
                throw new EntityNotFoundException(String.format("Role with ID %d not found!", roleId));
            }
            return role;
        }
    }

    @Override
    public List<Beer> top3MostRankedUserBeers(long userId) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, userId);
            if ((user == null) || (!user.getActive())) {
                throw new EntityNotFoundException(
                        String.format("User with ID %d not found!", userId));
            }
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> criteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> beerRoot = criteriaQuery.from(Beer.class);
            criteriaQuery = criteriaQuery.select(beerRoot);

            List<Predicate> wherePredicates = new ArrayList<>();
            wherePredicates.add(criteriaBuilder.equal(beerRoot.get("status"), StatusType.ACTIVE));
            Join<Beer, Rating> ratingJoin = beerRoot.join("ratings", JoinType.INNER);
            wherePredicates.add(criteriaBuilder.equal(ratingJoin.get("user"), user));

            criteriaQuery = criteriaQuery.where(wherePredicates.toArray(Predicate[]::new));
            criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.desc(ratingJoin.get("rating")));

            return session.createQuery(criteriaQuery).setMaxResults(3).getResultList();
        }
    }
}