package com.telerikacademy.team10.beer_tag.repositories.contracts;

import com.telerikacademy.team10.beer_tag.models.Brewery;

import java.util.List;
import java.util.Optional;

public interface BreweryRepository {

    List<Brewery> getBreweries();

    Optional<Brewery> getBreweryById(long id);

    void createBrewery(Brewery brewery);

    void updateBrewery(Brewery brewery);

    void deleteBrewery(long id);

    Optional<Brewery> getBreweryByName(String name);

}
