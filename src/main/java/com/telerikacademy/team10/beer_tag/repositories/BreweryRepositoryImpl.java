package com.telerikacademy.team10.beer_tag.repositories;

import com.telerikacademy.team10.beer_tag.models.Brewery;
import com.telerikacademy.team10.beer_tag.models.enums.StatusType;
import com.telerikacademy.team10.beer_tag.repositories.contracts.BreweryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brewery> getBreweries() {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Brewery> criteriaQuery = criteriaBuilder.createQuery(Brewery.class);
            Root<Brewery> breweryRoot = criteriaQuery.from(Brewery.class);
            criteriaQuery = criteriaQuery.select(breweryRoot).where(criteriaBuilder.equal(breweryRoot.get("status"), StatusType.ACTIVE));

            return session.createQuery(criteriaQuery).getResultList();
        }
    }

    @Override
    public Optional<Brewery> getBreweryById(long id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            return Optional.ofNullable(brewery);
        }
    }

    @Override
    public void createBrewery(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }

    }

    @Override
    public void updateBrewery(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.update(brewery);
                tx.commit();
            } catch (RuntimeException e) {
                if (tx != null) tx.rollback();
            }
        }
    }

    @Override
    public void deleteBrewery(long id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                Brewery brewery = session.get(Brewery.class, id);
                brewery.setStatus(StatusType.INACTIVE);
                session.update(brewery);
                tx.commit();
            } catch (RuntimeException e) {
                if (tx != null) tx.rollback();
            }
        }

    }

    @Override
    public Optional<Brewery> getBreweryByName(String name) {
        try (Session session = sessionFactory.openSession()) {

            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Brewery> criteriaQuery = criteriaBuilder.createQuery(Brewery.class);
            Root<Brewery> breweryRoot = criteriaQuery.from(Brewery.class);
            criteriaQuery = criteriaQuery.select(breweryRoot);

            List<Predicate> wherePredicates = new ArrayList<>();
            wherePredicates.add(criteriaBuilder.equal(breweryRoot.get("name"), name));
            wherePredicates.add(criteriaBuilder.equal(breweryRoot.get("status"), StatusType.ACTIVE));
            criteriaQuery = criteriaQuery.where(wherePredicates.toArray(Predicate[]::new));
            Query<Brewery> query = session.createQuery(criteriaQuery);

            return query.uniqueResultOptional();
        }
    }
}
