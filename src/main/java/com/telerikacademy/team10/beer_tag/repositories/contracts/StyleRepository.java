package com.telerikacademy.team10.beer_tag.repositories.contracts;

import com.telerikacademy.team10.beer_tag.models.Style;

import java.util.List;
import java.util.Optional;

public interface StyleRepository {

    List<Style> getAllStyles();

    Style getStyleById(long id);

    Optional<Style> getStyleByName(String name);

    void create(Style style);

    void update(Style style);

    void delete(long id);

    List<Style> filterByName(Optional<String> name);

    boolean existsByName(String name);
}
