package com.telerikacademy.team10.beer_tag.repositories.contracts;

import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.Rating;
import com.telerikacademy.team10.beer_tag.models.enums.FilterType;
import com.telerikacademy.team10.beer_tag.models.enums.SortType;

import java.util.List;
import java.util.Optional;

public interface BeerRepository {

    List<Beer> getBeers(FilterType filterType, String filterKey, SortType sortType);

    Optional<Beer> getBeerById(long id);

    Optional<Beer> getBeerByName(String name);

    void createBeer(Beer beer);

    void updateBeer(Beer beer);

    void deleteBeer(long id);

    Optional<Rating> getBeerRating(long userId, long beerId);

}
