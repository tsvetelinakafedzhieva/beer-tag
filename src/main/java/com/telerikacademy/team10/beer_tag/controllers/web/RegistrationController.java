package com.telerikacademy.team10.beer_tag.controllers.web;

import com.telerikacademy.team10.beer_tag.dto.CreateUserDTO;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.models.enums.RoleType;
import com.telerikacademy.team10.beer_tag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {

    private final UserDetailsManager userDetailsManager;
    private final UserService userService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new CreateUserDTO());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(
            @Valid @ModelAttribute CreateUserDTO userDto,
            BindingResult bindingResult,
            Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("user", userDto);
            model.addAttribute("error", "Incorrect input!");
            return "register";
        }
        if (userDetailsManager.userExists(userDto.getEmail())) {
            model.addAttribute("user", userDto);
            model.addAttribute("error", "User with the same email already exists!");
            return "register";
        }
        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            model.addAttribute("user", userDto);
            model.addAttribute("error", "Password confirmation doesn't match password!");
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(RoleType.ROLE_REGISTERED.name());
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getEmail(), "{noop}" + userDto.getPassword(), authorities);
        userDetailsManager.createUser(newUser);

        User userDetails = new User();
        userDetails.setFirstName(userDto.getFirstName());
        userDetails.setLastName(userDto.getLastName());
        userDetails.setEmail(userDto.getEmail());
        userDetails.setPhoto(userDto.getPhoto());
        userService.create(userDetails);

        return "register-confirmation";
    }
}
