package com.telerikacademy.team10.beer_tag.controllers.rest;

import com.telerikacademy.team10.beer_tag.dto.CreateUserDTO;
import com.telerikacademy.team10.beer_tag.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.exceptions.InvalidOperationException;
import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.Role;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.models.enums.RoleType;
import com.telerikacademy.team10.beer_tag.services.contracts.BeerService;
import com.telerikacademy.team10.beer_tag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final BeerService beerService;

    @Autowired
    public UserRestController(UserService userService, BeerService beerService) {
        this.userService = userService;
        this.beerService = beerService;
    }

    @GetMapping
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable long id) {
        try {
            return userService.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{email}")
    public User getByEmail(@PathVariable String email) {
        try {
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> search(@RequestParam(required = false) String email) {
        try {
            return userService.filterByEmail(Optional.ofNullable(email));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public User create(@Valid @RequestBody CreateUserDTO dto) {
        try {
            User user = new User();
            User newUser = mapUser(dto, user);
            userService.create(newUser);
            return newUser;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@PathVariable long id,
                       @Valid @RequestBody CreateUserDTO dto,
                       Principal principal) {
        try {
            User authorized = userService.getByEmail(principal.getName());
            validatePermission(id, authorized.getId());

            User userToUpdate = mapUser(dto, userService.getUserById(id));
            userService.update(id, userToUpdate);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id, Principal principal) {
        try {
            User authorizedUser = userService.getByEmail(principal.getName());
            validateDeletePermission(id, authorizedUser.getId());

            userService.delete(id);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("{id}/wishList")
    public Set<Beer> getWishList(@PathVariable long id, Principal principal) {
        try {
            User authorized = userService.getByEmail(principal.getName());
            validatePermission(id, authorized.getId());

            return userService.getWishBeers(id);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("{id}/drankList")
    public Set<Beer> getDrankList(@PathVariable long id, Principal principal) {
        try {
            User authorized = userService.getByEmail(principal.getName());
            validatePermission(id, authorized.getId());

            return userService.getDrankBeers(id);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/wishBeer")
    public void addBeerToWishList(@Valid @RequestBody long beerId, Principal principal) {
        try {
            User user = userService.getByEmail(principal.getName());
            userService.addBeerToWishList(user.getId(), beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/drankBeer")
    public void addBeerToDrankList(@Valid @RequestBody long beerId, Principal principal) {
        try {
            User user = userService.getByEmail(principal.getName());
            userService.addBeerToDrankList(user.getId(), beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/wishBeer")
    public void deleteWishBeer(@Valid @RequestBody long beerId, Principal principal) {
        try {
            User user = userService.getByEmail(principal.getName());
            userService.removeWishBeer(user.getId(), beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/drankBeer")
    public void deleteDrankBeer(@Valid @RequestBody long beerId, Principal principal) {
        try {
            User user = userService.getByEmail(principal.getName());
            userService.removeDrankBeer(user.getId(), beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/beers/top3")
    public List<Beer> getTop3MostRankedUserBeers(@PathVariable long id) {
        try {
            return userService.getTop3MostRankedUserBeers(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private void validateDeletePermission(long id, long userId) {
        User loggedUser = getUserById(id);
        User authorizedUser = userService.getUserById(userId);
        Set<Role> roles = authorizedUser.getRoles();
        Optional<Role> stream = roles
                .stream()
                .filter(role -> role.getRole() == RoleType.ROLE_ADMINISTRATOR)
                .findFirst();
        if (stream.isEmpty() || (loggedUser.getId() == authorizedUser.getId())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "You need permission to perform this action!");
        }
    }

    private void validatePermission(long id, long userId) {
        User loggedUser = getUserById(id);
        User authorizedUser = userService.getUserById(userId);
        Set<Role> roles = authorizedUser.getRoles();
        Optional<Role> stream = roles
                .stream()
                .filter(role -> role.getRole() == RoleType.ROLE_ADMINISTRATOR)
                .findFirst();
        if (stream.isEmpty() && (loggedUser.getId() != authorizedUser.getId())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "You need permission to perform this action!");
        }
    }

    private User mapUser(CreateUserDTO dto, User user) {
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setPhoto(dto.getPhoto());
        return user;
    }
}
