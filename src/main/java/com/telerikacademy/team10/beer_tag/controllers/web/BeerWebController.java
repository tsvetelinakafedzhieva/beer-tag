package com.telerikacademy.team10.beer_tag.controllers.web;

import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.Rating;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.services.contracts.BeerService;
import com.telerikacademy.team10.beer_tag.services.contracts.BreweryService;
import com.telerikacademy.team10.beer_tag.services.contracts.StyleService;
import com.telerikacademy.team10.beer_tag.services.contracts.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/beers")
public class BeerWebController {

    private final BeerService beerService;
    private final StyleService styleService;
    private final BreweryService breweryService;
    private final UserService userService;

    public BeerWebController(BeerService beerService,
                             StyleService styleService,
                             BreweryService breweryService,
                             UserService userService) {
        this.beerService = beerService;
        this.styleService = styleService;
        this.breweryService = breweryService;
        this.userService = userService;
    }

    @GetMapping
    public String getBeers(
            @RequestParam(required = false) String filterType,
            @RequestParam(required = false) String filterKey,
            @RequestParam(required = false) String sortType,
            Model model
    ) {
        List<Beer> beers = beerService.getBeers(
                Optional.ofNullable(filterType),
                Optional.ofNullable(filterKey),
                Optional.ofNullable(sortType)
        );

        model.addAttribute("beers", beers);
        return "beers";
    }

    @GetMapping("/{beerId}")
    public String getBeerDetails(@PathVariable long beerId, Model model, Authentication authentication) {
        Beer beer = beerService.getBeerById(beerId);
        model.addAttribute("beer", beer);

        if (authentication != null && authentication.isAuthenticated()) {
            User user = userService.getByEmail(authentication.getName());
            Rating rating = beerService.getBeerRating(user.getId(), beerId);
            model.addAttribute("rating", rating);

            boolean isAlreadyWished = userService.isAlreadyWished(beerId, user.getId());
            model.addAttribute("isWished", isAlreadyWished);

            boolean isAlreadyDrank = userService.isAlreadyDrank(beerId, user.getId());
            model.addAttribute("isDrank", isAlreadyDrank);
        }

        return "display-beer";
    }

    @PostMapping("/{beerId}/delete")
    public String deleteBeer(@PathVariable long beerId, Model model, Principal principal) {
        User user = userService.getByEmail(principal.getName());
        beerService.deleteBeer(beerId, user.getId());

        List<Beer> beers = beerService.getBeers(
                Optional.empty(),
                Optional.empty(),
                Optional.empty()
        );

        model.addAttribute("beers", beers);

        return "redirect:/beers";
    }

    @GetMapping("/new")
    public String getCreateBeer(Model model) {

        model.addAttribute("styles", styleService.getAllStyles());
        model.addAttribute("breweries", breweryService.getBreweries());
        return "create-beer";
    }

    @GetMapping("/{beerId}/update")
    public String getUpdateBeer(@PathVariable long beerId, Model model) {
        Beer beer = beerService.getBeerById(beerId);
        model.addAttribute("beer", beer);
        model.addAttribute("styles", styleService.getAllStyles());
        model.addAttribute("breweries", breweryService.getBreweries());

        return "create-beer";
    }

}
