package com.telerikacademy.team10.beer_tag.controllers.rest;

import com.telerikacademy.team10.beer_tag.dto.AddBeerRating;
import com.telerikacademy.team10.beer_tag.dto.CreateBeerRequest;
import com.telerikacademy.team10.beer_tag.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.exceptions.FileStorageException;
import com.telerikacademy.team10.beer_tag.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.beer_tag.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.Rating;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.services.contracts.BeerService;
import com.telerikacademy.team10.beer_tag.services.contracts.FileService;
import com.telerikacademy.team10.beer_tag.services.contracts.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/beers")
public class BeerController {

    private final BeerService service;
    private final UserService userService;
    private final FileService fileService;

    @Autowired
    public BeerController(BeerService service, UserService userService, FileService fileService) {
        this.service = service;
        this.userService = userService;
        this.fileService = fileService;
    }

    @GetMapping
    public List<Beer> getBeers(
            @RequestParam(required = false) String filterType,
            @RequestParam(required = false) String filterKey,
            @RequestParam(required = false) String sortType) {
        try {
            return service.getBeers(
                    Optional.ofNullable(filterType),
                    Optional.ofNullable(filterKey),
                    Optional.ofNullable(sortType)
            );
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Beer getBeerById(@PathVariable long id) {
        try {
            return service.getBeerById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(consumes = {"multipart/form-data"}, produces = "application/json")
    public Beer createBeer(@RequestPart("beerData") @Valid CreateBeerRequest request,
                           @RequestPart("beerImage") @Valid @Nullable MultipartFile file,
                           Principal principal) {
        try {
            String userEmail = principal.getName();
            User user = userService.getByEmail(userEmail);

            if (file != null) {
                addFileToRequest(request, file);
            }

            return service.createBeer(request, user.getId());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    private void addFileToRequest(CreateBeerRequest request, @NonNull MultipartFile file) {
        String fileDownloadUri;
        try {
            String fileName = fileService.storeFile(file);
            fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/files/get/")
                    .path(fileName)
                    .toUriString();
        } catch (FileStorageException e) {
            fileDownloadUri = null;
        }

        request.setPictureUrl(fileDownloadUri);
    }

    @PutMapping(path = "/{id}", consumes = {"multipart/form-data"}, produces = "application/json")
    public Beer updateBeer(@PathVariable long id,
                           @RequestPart("beerData") @Valid CreateBeerRequest request,
                           @RequestPart("beerImage") @Valid @Nullable MultipartFile file,
                           Principal principal) {
        try {
            if (file != null) {
                addFileToRequest(request, file);
            }
            User user = userService.getByEmail(principal.getName());
            return service.updateBeer(id, request, user.getId());
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedAccessException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id, Principal principal) {
        try {
            User user = userService.getByEmail(principal.getName());
            service.deleteBeer(id, user.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedAccessException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PostMapping("/{id}/rate")
    public Beer changeRating(@PathVariable long id,
                             Principal principal,
                             @Valid @RequestBody AddBeerRating request) {
        try {
            User user = userService.getByEmail(principal.getName());
            return service.addRating(id, user.getId(), request.getRating());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/rating/")
    public Rating getBeerRating(@PathVariable long id,
                                Principal principal) {
        try {
            User user = userService.getByEmail(principal.getName());
            return service.getBeerRating(user.getId(), id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
