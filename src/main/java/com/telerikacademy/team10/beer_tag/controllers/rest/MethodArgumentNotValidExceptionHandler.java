package com.telerikacademy.team10.beer_tag.controllers.rest;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = RestController.class)
public class MethodArgumentNotValidExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ValidationError methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<org.springframework.validation.FieldError> fieldErrors = result.getFieldErrors();
        return processFieldErrors(fieldErrors);
    }

    private ValidationError processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors) {
        ValidationError error = new ValidationError(HttpStatus.BAD_REQUEST.value(), "Validation Error");
        for (org.springframework.validation.FieldError fieldError : fieldErrors) {
            error.addFieldError(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return error;
    }

    static class ValidationError {
        private final int status;
        private final String message;
        private final List<FieldError> fieldErrors;

        ValidationError(int status, String message) {
            this.status = status;
            this.message = message;
            this.fieldErrors = new ArrayList<>();
        }

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        void addFieldError(String field, String message) {
            FieldError error = new FieldError(field, message);
            fieldErrors.add(error);
        }

        public List<FieldError> getFieldErrors() {
            return fieldErrors;
        }
    }

    static class FieldError {

        private final String field;
        private final String message;

        FieldError(String path, String message) {
            this.field = path;
            this.message = message;
        }

        public String getField() {
            return field;
        }

        public String getMessage() {
            return message;
        }
    }
}