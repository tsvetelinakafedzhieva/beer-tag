package com.telerikacademy.team10.beer_tag.controllers.web;

import com.telerikacademy.team10.beer_tag.dto.UpdateUserDto;
import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.exceptions.FileStorageException;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.services.contracts.FileService;
import com.telerikacademy.team10.beer_tag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping
public class UserController {

    private final UserDetailsManager userDetailsManager;
    private final UserService userService;
    private final FileService fileService;

    @Autowired
    public UserController(UserDetailsManager userDetailsManager, UserService userService, FileService fileService) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
        this.fileService = fileService;
    }

    @GetMapping("/users")
    public String showUsers(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return "users";
    }

    @GetMapping("/profile")
    public String showProfile(Model model, Principal principal) {
        User user = userService.getByEmail(principal.getName());
        model.addAttribute("user", user);
        model.addAttribute("wishList", userService.getWishBeers(user.getId()));
        model.addAttribute("drankList", userService.getDrankBeers(user.getId()));
        model.addAttribute("topRankedBeers", userService.getTop3MostRankedUserBeers(user.getId()));
        return "get-user";
    }

    @GetMapping("/users/{id}")
    public String showUserPage(@PathVariable long id, Model model) {
        User user;
        try {
            user = userService.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        model.addAttribute("user", user);
        model.addAttribute("wishList", userService.getWishBeers(id));
        model.addAttribute("drankList", userService.getDrankBeers(id));
        model.addAttribute("topRankedBeers", userService.getTop3MostRankedUserBeers(id));
        return "get-user";
    }

    @GetMapping("/users/{id}/update")
    public String showUpdateUserPage(@PathVariable long id, Model model) {
        User user = userService.getUserById(id);
        model.addAttribute("user", user);
        model.addAttribute("userDto", new UpdateUserDto());


        return "update-user";
    }

    @PostMapping("/users/{id}/update")
    public String updateUser(@PathVariable long id,
                             @Valid @ModelAttribute UpdateUserDto dto,
                             Principal principal,
                             BindingResult result,
                             Model model) {

        User userToUpdate = userService.getUserById(id);

        if (result.hasErrors()) {
            model.addAttribute("user", userToUpdate);
            model.addAttribute("userDto", dto);
            model.addAttribute("error", "Email & password can't be empty!");
            return "update-user";
        }


        if (!isNullOrEmpty(dto.getPassword()) && !isNullOrEmpty(dto.getConfirmedPassword()) && !isNullOrEmpty(dto.getOldPassword())) {

            if (!(dto.getPassword().equals(dto.getConfirmedPassword()))) {
                model.addAttribute("user", userToUpdate);
                model.addAttribute("userDto", dto);
                model.addAttribute("error", "Password and password confirmation doesn't match!");
                return "update-user";
            }

            UserDetails userDetails = userDetailsManager.loadUserByUsername(principal.getName());

            String oldPassword = String.format("{noop}%s", dto.getOldPassword());
            if (!(oldPassword.equals(userDetails.getPassword()))) {
                model.addAttribute("user", userToUpdate);
                model.addAttribute("userDto", dto);
                model.addAttribute("error", "Old password doesn't match!");
                return "update-user";
            }

            String newPassword = String.format("{noop}%s", dto.getPassword());
            userDetailsManager.changePassword(dto.getOldPassword(), newPassword);

        }

        userToUpdate.setFirstName(dto.getFirstName());
        userToUpdate.setLastName(dto.getLastName());

        if (dto.getPhoto() != null) {
            addFileToUser(userToUpdate, dto.getPhoto());
        }

        userService.update(userToUpdate.getId(), userToUpdate);

        return "redirect:/";
    }

    @PostMapping("/users/{id}/delete")
    public String deleteUser(@PathVariable long id, Model model, Principal principal) {
        userDetailsManager.deleteUser(userService.getUserById(id).getEmail());
        userService.delete(id);

        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);

        return "redirect:/";
    }

    private boolean isNullOrEmpty(String text) {
        return text == null || text.isEmpty();
    }

    private void addFileToUser(User user, @NonNull MultipartFile file) {
        String fileDownloadUri;
        try {
            String fileName = fileService.storeFile(file);
            fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/files/get/")
                    .path(fileName)
                    .toUriString();
        } catch (FileStorageException e) {
            fileDownloadUri = null;
        }

        user.setPhoto(fileDownloadUri);
    }
}
