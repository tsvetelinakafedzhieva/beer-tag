package com.telerikacademy.team10.beer_tag.controllers.rest;

import com.telerikacademy.team10.beer_tag.dto.CreateBreweryRequest;
import com.telerikacademy.team10.beer_tag.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.beer_tag.models.Brewery;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.services.contracts.BreweryService;
import com.telerikacademy.team10.beer_tag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryController {
    private final BreweryService service;
    private final UserService userService;

    @Autowired
    public BreweryController(BreweryService service, UserService userService) {
        this.service = service;
        this.userService = userService;

    }

    @GetMapping
    public List<Brewery> getBreweries() {
        return service.getBreweries();
    }

    @GetMapping("/{id}")
    public Brewery getBreweryById(@PathVariable long id) {
        try {
            return service.getBreweryById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Brewery createBrewery(Principal principal,
                                 @Valid @RequestBody CreateBreweryRequest request) {
        try {
            User user = userService.getByEmail(principal.getName());
            return service.createBrewery(request, user.getId());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brewery updateBrewery(@PathVariable long id,
                                 Principal principal,
                                 @Valid @RequestBody CreateBreweryRequest request) {
        try {
            User user = userService.getByEmail(principal.getName());
            return service.updateBrewery(id, request, user.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedAccessException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id,
                       Principal principal) {
        try {
            User user = userService.getByEmail(principal.getName());
            service.deleteBrewery(id, user.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedAccessException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
