package com.telerikacademy.team10.beer_tag.controllers.rest;

import com.telerikacademy.team10.beer_tag.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.models.Role;
import com.telerikacademy.team10.beer_tag.models.Style;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.models.enums.RoleType;
import com.telerikacademy.team10.beer_tag.services.contracts.StyleService;
import com.telerikacademy.team10.beer_tag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {

    private final StyleService styleService;
    private final UserService userService;

    @Autowired
    public StyleRestController(StyleService styleService, UserService userService) {
        this.styleService = styleService;
        this.userService = userService;
    }

    @GetMapping
    public List<Style> getAllStyles() {
        return styleService.getAllStyles();
    }

    @GetMapping("/{id}")
    public Style getStyleById(@PathVariable int id) {
        try {
            return styleService.getStyleById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<Style> searchByName(@RequestParam(required = false) String name) {
        try {
            return styleService.filterByName(Optional.ofNullable(name));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public Style create(Principal principal,
                        @Valid @RequestBody Style style) {
        try {
            User authorizedUser = userService.getByEmail(principal.getName());
            return styleService.create(style, authorizedUser.getId());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Style update(@PathVariable long id,
                        Principal principal,
                        @Valid @RequestBody Style style) {
        try {
            User authorizedUser = userService.getByEmail(principal.getName());
            return styleService.update(id, style, authorizedUser.getId());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id,
                       Principal principal) {
        try {
            User authorizedUser = userService.getByEmail(principal.getName());
            styleService.delete(id, authorizedUser.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    private void validateUserAdminRole(long userId) {
        User user = userService.getUserById(userId);
        Set<Role> rolesToStream = user.getRoles();
        Optional<Role> stream = rolesToStream
                .stream()
                .filter(role -> role.getRole() == RoleType.ROLE_ADMINISTRATOR)
                .findFirst();
        if (stream.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "You need permission to perform this action!");
        }
    }
}