package com.telerikacademy.team10.beer_tag.models.enums;

public enum StatusType {
    ACTIVE,
    INACTIVE
}
