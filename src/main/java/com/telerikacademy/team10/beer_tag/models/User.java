package com.telerikacademy.team10.beer_tag.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users_details")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Email
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "picture_url")
    private String photo;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "auth_id", referencedColumnName = "auth_id")
    )
    private Set<Role> roles;

    @JsonIgnore
    @Column(name = "is_active")
    private Boolean active = true;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_wish_list",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> usersWishList;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_drank_list",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> usersDrankList;

    public User() {
    }

    public User(String firstName, String lastName, String email, String pictureUrl) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.photo = pictureUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String name) {
        this.firstName = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Role> getRoles() {
        if (roles == null) {
            roles = new HashSet<>();
        }
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(@Nullable String pictureUrl) {
        this.photo = Objects.requireNonNullElse(pictureUrl,
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSuzcAehjxzaTPVUj8halLVdiGpJqfRsAbSdQ&usqp=CAU");
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Beer> getUsersWishList() {
        return Collections.unmodifiableSet(usersWishList);
    }

    public void addBeerToWishList(Beer wishBeer) {
        usersWishList.add(wishBeer);
    }

    public void removeWishBeer(Beer beer) {
        usersWishList.remove(beer);
    }

    public Set<Beer> getUsersDrankList() {
        return Collections.unmodifiableSet(usersDrankList);
    }

    public void addBeerToDrankList(Beer drankBeer) {
        usersDrankList.add(drankBeer);
    }

    public void removeDrankBeer(Beer beer) {
        usersDrankList.remove(beer);
    }
}