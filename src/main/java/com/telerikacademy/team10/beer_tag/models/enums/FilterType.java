package com.telerikacademy.team10.beer_tag.models.enums;

import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;

public enum FilterType {
    COUNTRY, STYLE, TAG, ANY;

    public static FilterType fromString(String type) {
        switch (type.toLowerCase()) {
            case "country":
                return COUNTRY;
            case "style":
                return STYLE;
            case "tag":
                return TAG;
            case "any":
                return ANY;
            default:
                throw new EntityNotFoundException("Filter type is unknown");
        }
    }
}
