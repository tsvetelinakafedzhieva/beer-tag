package com.telerikacademy.team10.beer_tag.models.enums;

public enum RoleType {
    ROLE_ADMINISTRATOR, ROLE_REGISTERED;
}
