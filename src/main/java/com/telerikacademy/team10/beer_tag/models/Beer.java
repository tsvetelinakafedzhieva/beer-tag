package com.telerikacademy.team10.beer_tag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.team10.beer_tag.models.enums.StatusType;
import org.hibernate.annotations.Formula;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "beers")
public class Beer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @Column(name = "abv_percent")
    private double abv;

    @Formula(value = "(select ifnull(avg(ur.rating), 0.0) from beers\n" +
            "inner join beer_user_rating bur on beers.beer_id = bur.beer_id\n" +
            "inner join user_ratings ur on bur.rating_id = ur.rating_id)")
    private double averageRating;

    @Column(name = "beer_picture_url")
    private String pictureUrl;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "beer_user_rating",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "rating_id"))
    private Set<Rating> ratings;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "beers_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private StatusType status;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    @JsonIgnore
    private User owner;

    public Beer() {
        ratings = new HashSet<>();
        averageRating = 0.0;
        status = StatusType.ACTIVE;
    }

    public Beer setId(long id) {
        this.id = id;
        return this;
    }

    public User getOwner() {
        return owner;
    }

    public Beer setOwner(User owner) {
        this.owner = owner;
        return this;
    }

    public StatusType getStatus() {
        return status;
    }

    public Beer setStatus(StatusType status) {
        this.status = status;
        return this;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Beer setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Beer setDescription(String description) {
        this.description = description;
        return this;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public Beer setBrewery(Brewery brewery) {
        this.brewery = brewery;
        return this;
    }

    public Style getStyle() {
        return style;
    }

    public Beer setStyle(Style style) {
        this.style = style;
        return this;
    }

    public double getAbv() {
        return abv;
    }

    public Beer setAbv(double abv) {
        this.abv = abv;
        return this;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public Set<Tag> getTags() {
        return Collections.unmodifiableSet(tags);
    }

    public Beer setTags(Set<Tag> tags) {
        this.tags = tags;
        return this;
    }

    @NonNull
    public String getPictureUrl() {
        return pictureUrl;
    }

    public Beer setPictureUrl(@Nullable String pictureUrl) {
        if (pictureUrl == null) {
            this.pictureUrl = "https://www.ibts.org/wp-content/uploads/2017/08/iStock-476085198.jpg";
        } else {
            this.pictureUrl = pictureUrl;
        }
        return this;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public Set<Rating> getRatings() {
        return Collections.unmodifiableSet(ratings);
    }

    public void addRating(Rating rating) {
        ratings.add(rating);
    }

    public void removeRating(Rating rating) {
        ratings.remove(rating);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beer beer = (Beer) o;
        return id == beer.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Beer setAverageRating(double averageRating) {
        this.averageRating = averageRating;
        return this;
    }

    public Beer setRatings() {
        this.ratings = new HashSet<>();
        return this;
    }
}
