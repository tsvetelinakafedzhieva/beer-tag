package com.telerikacademy.team10.beer_tag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.team10.beer_tag.models.enums.StatusType;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name = "breweries")
public class Brewery {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "brewery_id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "origin_country")
    private String originCountry;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private StatusType status;

    public Brewery() {
        status = StatusType.ACTIVE;
    }

    public Brewery(String name, String originCountry) {
        setName(name);
        setOriginCountry(originCountry);
        status = StatusType.ACTIVE;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Brewery brewery = (Brewery) o;
        return id == brewery.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
