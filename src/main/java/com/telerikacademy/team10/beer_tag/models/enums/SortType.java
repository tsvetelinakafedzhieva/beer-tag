package com.telerikacademy.team10.beer_tag.models.enums;

import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;

public enum SortType {
    NAME, ABV, RATING, ANY;

    public static SortType fromString(String type) {
        switch (type.toLowerCase()) {
            case "name":
                return NAME;
            case "abv":
                return ABV;
            case "rating":
                return RATING;
            case "any":
                return ANY;
            default:
                throw new EntityNotFoundException("Sort type unknown");
        }
    }
}
