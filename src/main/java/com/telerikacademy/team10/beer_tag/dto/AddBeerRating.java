package com.telerikacademy.team10.beer_tag.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class AddBeerRating {

    private static final String VALIDATION_MESSAGE = "Must be between 1 and 5";

    @Min(value = 1, message = VALIDATION_MESSAGE)
    @Max(value = 5, message = VALIDATION_MESSAGE)
    private int rating;

    public AddBeerRating() {
    }

    public AddBeerRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }
}
