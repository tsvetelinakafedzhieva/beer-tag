package com.telerikacademy.team10.beer_tag.dto;

import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;

public class UpdateUserDto {

    private static final String NAME_LENGTH_MESSAGE =
            "Name should be between 3 and 50 symbols.";
    private static final String EMAIL_LENGTH_MESSAGE =
            "User email should be between 10 and 50 symbols.";

    @Size(min = 3, max = 50, message = NAME_LENGTH_MESSAGE)
    private String firstName;

    @Size(min = 3, max = 50, message = NAME_LENGTH_MESSAGE)
    private String lastName;

    @Nullable
    private String oldPassword;

    @Nullable
    private String password;

    @Nullable
    private String confirmedPassword;

    @Nullable
    private MultipartFile photo;



    public UpdateUserDto() {
    }

    @Nullable
    public String getOldPassword() {
        return oldPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getPassword() {
        return password;
    }


    @Nullable
    public MultipartFile getPhoto() {
        return photo;
    }

    @Nullable
    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setOldPassword(@Nullable String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    public void setConfirmedPassword(@Nullable String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }

    public void setPhoto(@Nullable MultipartFile photo) {
        this.photo = photo;
    }
}
