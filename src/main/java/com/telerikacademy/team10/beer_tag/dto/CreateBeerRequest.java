package com.telerikacademy.team10.beer_tag.dto;

import org.springframework.lang.Nullable;

import java.util.List;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;


public class CreateBeerRequest {

    @Size(min = 2, max = 50, message = "Beer's name must be between 2 and 50 symbols long.")
    private  String name;
    @Size(min = 10, max = 500, message = "Beer's description must be between 10 and 500 symbols long.")
    private String description;
    @Positive
    private long breweryId;
    @Positive
    private int styleId;
    @PositiveOrZero
    private double abv;

    @Nullable
    private  String pictureUrl;
    @Nullable
    private  List<String> tags;

    public CreateBeerRequest(String name, String description, long breweryId, int styleId, double abv, @Nullable String picture, @Nullable List<String> tags) {
        this.name = name;
        this.description = description;
        this.breweryId = breweryId;
        this.styleId = styleId;
        this.abv = abv;
        this.pictureUrl = picture;
        this.tags = tags;
    }

    public CreateBeerRequest() {

    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public long getBreweryId() {
        return breweryId;
    }

    public int getStyleId() {
        return styleId;
    }

    public double getAbv() {
        return abv;
    }

    public @Nullable
    String getPictureUrl() {
        return pictureUrl;
    }

    public @Nullable
    List<String> getTags() {
        return tags;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBreweryId(long breweryId) {
        this.breweryId = breweryId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public void setPictureUrl(@Nullable String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public void setTags(@Nullable List<String> tags) {
        this.tags = tags;
    }
}
