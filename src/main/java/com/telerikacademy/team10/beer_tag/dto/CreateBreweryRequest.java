package com.telerikacademy.team10.beer_tag.dto;

import javax.validation.constraints.Size;

public class CreateBreweryRequest {


    @Size(min = 2, max = 50, message = "The brewery's name must be between 2 and 50 symbols long.")
    private String name;
    @Size(min = 2, max = 50, message = "The brewery's name must be between 2 and 50 symbols long.")
    private String country;

    public CreateBreweryRequest() {
    }

    public CreateBreweryRequest(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }
}
