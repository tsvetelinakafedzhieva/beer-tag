package com.telerikacademy.team10.beer_tag.services.contracts;

import com.telerikacademy.team10.beer_tag.models.Tag;

public interface TagService {

    Tag getOrCreate(String name);
}
