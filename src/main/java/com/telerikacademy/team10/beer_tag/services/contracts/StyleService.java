package com.telerikacademy.team10.beer_tag.services.contracts;

import com.telerikacademy.team10.beer_tag.models.Style;

import java.util.List;
import java.util.Optional;

public interface StyleService {

    List<Style> getAllStyles();

    Style getStyleById(long id);

    Optional<Style> getStyleByName(String name);

    List<Style> filterByName(Optional<String> name);

    Style create(Style style, long userId);

    Style update(long id, Style style, long userId);

    void delete(long id, long userId);
}
