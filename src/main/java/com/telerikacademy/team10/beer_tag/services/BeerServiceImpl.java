package com.telerikacademy.team10.beer_tag.services;

import com.telerikacademy.team10.beer_tag.dto.CreateBeerRequest;
import com.telerikacademy.team10.beer_tag.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.beer_tag.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.beer_tag.models.*;
import com.telerikacademy.team10.beer_tag.models.enums.FilterType;
import com.telerikacademy.team10.beer_tag.models.enums.RoleType;
import com.telerikacademy.team10.beer_tag.models.enums.SortType;
import com.telerikacademy.team10.beer_tag.models.enums.StatusType;
import com.telerikacademy.team10.beer_tag.repositories.contracts.*;
import com.telerikacademy.team10.beer_tag.services.contracts.BeerService;
import com.telerikacademy.team10.beer_tag.services.contracts.TagService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BeerServiceImpl implements BeerService {

    private final BeerRepository beerRepository;
    private final BreweryRepository breweryRepository;
    private final StyleRepository styleRepository;
    private final TagService tagService;
    private final UserRepository userRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository,
                           BreweryRepository breweryRepository,
                           StyleRepository styleRepository,
                           TagService tagService,
                           UserRepository userRepository
    ) {
        this.beerRepository = beerRepository;
        this.breweryRepository = breweryRepository;
        this.styleRepository = styleRepository;
        this.tagService = tagService;
        this.userRepository = userRepository;
    }

    @Override
    public List<Beer> getBeers(Optional<String> filterType, Optional<String> filterKey, Optional<String> sortType) {
        if (filterKey.isPresent() && filterType.isEmpty() || filterKey.isEmpty() && filterType.isPresent()) {
            throw new InvalidArgumentException("Required both type and key for filter actions.");
        }

        SortType sort = SortType.ANY;
        if (sortType.isPresent()) {
            sort = SortType.fromString(sortType.get());
        }

        FilterType filter = FilterType.ANY;
        String filterValue = "";
        if (filterType.isPresent()) {
            filter = FilterType.fromString(filterType.get());
            filterValue = filterKey.get();
        }

        return beerRepository.getBeers(filter, filterValue, sort);
    }

    @Override
    public Beer getBeerById(long id) {
        Optional<Beer> beer = beerRepository.getBeerById(id);
        if (beer.isEmpty() || beer.get().getStatus() == StatusType.INACTIVE) {
            throw new EntityNotFoundException(String.format("Beer with ID %d not found!", id));
        }
        return beer.get();
    }

    @Override
    public Beer createBeer(CreateBeerRequest request, long ownerId) {
        Optional<Beer> beer = beerRepository.getBeerByName(request.getName());
        if (beer.isPresent()) {
            throw new DuplicateEntityException(String.format("Beer with name %s already exists!", request.getName()));
        }

        Beer newBeer = mapBeer(new Beer(), ownerId, request);

        beerRepository.createBeer(newBeer);
        return newBeer;
    }

    @Override
    public Beer updateBeer(long id, CreateBeerRequest request, long ownerId) {
        Beer original = validateBeerOperation(id, ownerId);
        Optional<Beer> beerByName = beerRepository.getBeerByName(request.getName());

        if (beerByName.isPresent() && beerByName.get().getId() != id) {
            throw new InvalidArgumentException(String.format("Beer with name %s already exists.", request.getName()));
        }

        Beer updatedBeer = mapBeer(original, original.getOwner().getId(), request);

        beerRepository.updateBeer(updatedBeer);

        return updatedBeer;
    }

    @Override
    public void deleteBeer(long id, long ownerId) {
        validateBeerOperation(id, ownerId);
        beerRepository.deleteBeer(id);
    }

    @Override
    public Beer addRating(long beerId, long userId, int currentRating) {
        Beer beer = getBeerById(beerId);
        Optional<Rating> previousRatingByUser = beer.getRatings().stream()
                .filter((rating -> rating.getUser().getId() == userId)).findFirst();

        Rating rating;
        if (previousRatingByUser.isPresent()) {
            rating = previousRatingByUser.get();
            beer.removeRating(rating);
            rating.setRating(currentRating);
        } else {
            User user = userRepository.getUserById(userId);
            rating = new Rating(user, currentRating);
        }

        beer.addRating(rating);
        beerRepository.updateBeer(beer);
        return getBeerById(beer.getId());
    }

    @Override
    public Rating getBeerRating(long userId, long beerId) {
        User user = userRepository.getUserById(userId);
        getBeerById(beerId);
        return beerRepository.getBeerRating(userId, beerId)
                .orElse(new Rating(user, 0));
    }

    @NonNull
    private Beer validateBeerOperation(long id, long ownerId) {
        Beer original = getBeerById(id);
        User user = original.getOwner();
        Optional<Role> userRole = userRepository.getUserById(ownerId).getRoles().stream()
                .filter(role -> role.getRole() == RoleType.ROLE_ADMINISTRATOR)
                .findFirst();
        if (user.getId() != ownerId && userRole.isEmpty()) {
            throw new UnauthorizedAccessException("You need permission to perform this action");
        }
        return original;
    }

    private Beer mapBeer(Beer original, long ownerId, CreateBeerRequest request) {
        Optional<Brewery> brewery = breweryRepository.getBreweryById(request.getBreweryId());
        if (brewery.isEmpty()) {
            throw new EntityNotFoundException(String.format("Brewery with ID %d doesn't exists", request.getBreweryId()));
        }
        Style style = styleRepository.getStyleById(request.getStyleId());

        Set<Tag> tags = new HashSet<>();
        if (request.getTags() != null) {
            tags = request.getTags().stream().map(tagService::getOrCreate).collect(Collectors.toUnmodifiableSet());
        }

        User owner = userRepository.getUserById(ownerId);

        return original.setName(request.getName())
                .setDescription(request.getDescription())
                .setBrewery(brewery.get())
                .setStyle(style)
                .setAbv(request.getAbv())
                .setPictureUrl(request.getPictureUrl())
                .setTags(tags)
                .setOwner(owner);
    }

}
