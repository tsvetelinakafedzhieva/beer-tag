package com.telerikacademy.team10.beer_tag.services;

import com.telerikacademy.team10.beer_tag.dto.CreateBreweryRequest;
import com.telerikacademy.team10.beer_tag.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.beer_tag.models.Brewery;
import com.telerikacademy.team10.beer_tag.models.Role;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.models.enums.RoleType;
import com.telerikacademy.team10.beer_tag.models.enums.StatusType;
import com.telerikacademy.team10.beer_tag.repositories.contracts.BreweryRepository;
import com.telerikacademy.team10.beer_tag.repositories.contracts.UserRepository;
import com.telerikacademy.team10.beer_tag.services.contracts.BreweryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class BreweryServiceImpl implements BreweryService {
    private final BreweryRepository breweryRepository;
    private final UserRepository userRepository;

    public BreweryServiceImpl(BreweryRepository breweryRepository, UserRepository userRepository) {
        this.breweryRepository = breweryRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Brewery> getBreweries() {
        return breweryRepository.getBreweries();
    }

    @Override
    public Brewery getBreweryById(long id) {
        Optional<Brewery> brewery = breweryRepository.getBreweryById(id);
        if (brewery.isEmpty() || brewery.get().getStatus() == StatusType.INACTIVE) {
            throw new EntityNotFoundException(String.format("Brewery with ID %d doesn't exist!", id));
        }
        return brewery.get();
    }

    @Override
    public Brewery createBrewery(CreateBreweryRequest request, long userId) {
        validatePermission(userId);
        Optional<Brewery> brewery = breweryRepository.getBreweryByName(request.getName());
        if (brewery.isPresent()) {
            throw new DuplicateEntityException(String.format("Brewery with name %s already exists!", request.getName()));
        }
        Brewery newBrewery = new Brewery(request.getName(), request.getCountry());
        breweryRepository.createBrewery(newBrewery);
        return newBrewery;
    }


    @Override
    public Brewery updateBrewery(long id, CreateBreweryRequest request, long userId) {
        validatePermission(userId);
        Brewery updatedBrewery = getBreweryById(id);
        Optional<Brewery> breweryByName = breweryRepository.getBreweryByName(request.getName());
        if (breweryByName.isPresent() && breweryByName.get().getId() != id) {
            throw new DuplicateEntityException(String.format("Brewery with name %s already exists!", request.getName()));
        }

        updatedBrewery.setName(request.getName());
        updatedBrewery.setOriginCountry(request.getCountry());

        breweryRepository.updateBrewery(updatedBrewery);
        return updatedBrewery;
    }

    @Override
    public void deleteBrewery(long id, long userId) {
        validatePermission(userId);
        getBreweryById(id);
        breweryRepository.deleteBrewery(id);

    }

    private void validatePermission(long userId) {
        User user = userRepository.getUserById(userId);
        Set<Role> roles = user.getRoles();
        Optional<Role> stream = roles.stream()
                .filter(role -> role.getRole() == RoleType.ROLE_ADMINISTRATOR)
                .findFirst();
        if (stream.isEmpty()) {
            throw new UnauthorizedAccessException("You need permission to perform this action");
        }
    }


}
