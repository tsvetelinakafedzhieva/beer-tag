package com.telerikacademy.team10.beer_tag.services;

import com.telerikacademy.team10.beer_tag.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.beer_tag.models.Style;
import com.telerikacademy.team10.beer_tag.repositories.contracts.StyleRepository;
import com.telerikacademy.team10.beer_tag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StyleServiceImpl implements StyleService {

    private final StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository repository) {
        this.styleRepository = repository;
    }

    @Override
    public List<Style> getAllStyles() {
        return styleRepository.getAllStyles();
    }

    @Override
    public Style getStyleById(long id) {
        return styleRepository.getStyleById(id);
    }

    @Override
    public Optional<Style> getStyleByName(String name) {
        return styleRepository.getStyleByName(name);
    }

    @Override
    public List<Style> filterByName(Optional<String> name) {
        return styleRepository.filterByName(name);
    }

    @Override
    public Style create(Style style, long userId) {
        validateUniqueStyleName(style.getName());
        styleRepository.create(style);
        return style;
    }

    @Override
    public Style update(long id, Style style, long userId) {
        getStyleById(id);
        Style updatedStyle = new Style(style.getName());
        updatedStyle.setId(id);
        styleRepository.update(updatedStyle);
        return updatedStyle;
    }

    @Override
    public void delete(long id, long userId) {
        getStyleById(id);
        styleRepository.delete(id);
    }

    private void validateUniqueStyleName(String name) {
        if (styleRepository.existsByName(name)) {
            throw new DuplicateEntityException(
                    String.format("Style with name %s already exists!", name));
        }
    }
}