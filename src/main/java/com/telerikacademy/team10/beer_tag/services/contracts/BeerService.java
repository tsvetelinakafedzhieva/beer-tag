package com.telerikacademy.team10.beer_tag.services.contracts;

import com.telerikacademy.team10.beer_tag.dto.CreateBeerRequest;
import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.Rating;

import java.util.List;
import java.util.Optional;

public interface BeerService {
    List<Beer> getBeers(Optional<String> filterType, Optional<String> filterKey, Optional<String> sortType);

    Beer getBeerById(long id);

    Beer createBeer(CreateBeerRequest request, long ownerId);

    Beer updateBeer(long id, CreateBeerRequest request, long ownerId);

    void deleteBeer(long id, long ownerId);

    Beer addRating(long beerId, long userId, int currentRating);

    Rating getBeerRating(long userId, long beerId);

}
