package com.telerikacademy.team10.beer_tag.services;

import com.telerikacademy.team10.beer_tag.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.beer_tag.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.Role;
import com.telerikacademy.team10.beer_tag.models.User;
import com.telerikacademy.team10.beer_tag.repositories.contracts.BeerRepository;
import com.telerikacademy.team10.beer_tag.repositories.contracts.UserRepository;
import com.telerikacademy.team10.beer_tag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BeerRepository beerRepository;

    int ROLE_REGISTERED = 2;
    int ROLE_ADMINISTRATOR = 1;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BeerRepository beerRepository) {
        this.userRepository = userRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public User getUserById(long id) {
        return userRepository.getUserById(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    @Transactional
    public User create(User user) {
        validateUniqueUserEmail(user.getEmail());
        Role role = userRepository.getRole(ROLE_REGISTERED);
        Set<Role> roles = user.getRoles();
        roles.add(role);
        userRepository.create(user);
        return user;
    }

    @Override
    @Transactional
    public void update(long id, User user) {
        validateUpdatedUserEmail(user.getEmail());

        userRepository.update(user);
    }

    @Override
    @Transactional
    public void delete(long id) {
        userRepository.delete(id);
    }

    @Override
    public List<User> filterByEmail(Optional<String> email) {
        return userRepository.filterByEmail(email);
    }

    @Override
    public Set<Beer> getWishBeers(long id) {
        User user = userRepository.getUserById(id);
        return user.getUsersWishList();
    }

    @Override
    public void addBeerToWishList(long userId, long beerId) {
        User user = getUserById(userId);
        Optional<Beer> beer = beerRepository.getBeerById(beerId);
        if (beer.isEmpty()) {
            throw new EntityNotFoundException((
                    String.format("Beer with ID %d not found!", beerId)));
        }
        user.addBeerToWishList(beer.get());
        userRepository.update(user);
    }

    @Override
    public void removeWishBeer(long userId, long beerId) {
        User user = getUserById(userId);
        Optional<Beer> beer = user.getUsersWishList()
                .stream()
                .filter(b -> b.getId() == beerId)
                .findFirst();
        if (beer.isEmpty()) {
            throw new EntityNotFoundException((
                    String.format("Beer with ID %d not found!", beerId)));
        }
        user.removeWishBeer(beer.get());
        userRepository.update(user);
    }

    @Override
    public Set<Beer> getDrankBeers(long id) {
        User user = userRepository.getUserById(id);
        return user.getUsersDrankList();
    }

    @Override
    public void addBeerToDrankList(long userId, long beerId) {
        User user = getUserById(userId);
        Optional<Beer> beer = beerRepository.getBeerById(beerId);
        if (beer.isEmpty()) {
            throw new EntityNotFoundException((
                    String.format("Beer with ID %d not found!", beerId)));
        }
        user.addBeerToDrankList(beer.get());
        userRepository.update(user);
    }

    @Override
    public void removeDrankBeer(long userId, long beerId) {
        User user = getUserById(userId);
        Optional<Beer> beer = user.getUsersDrankList()
                .stream()
                .filter(b -> b.getId() == beerId)
                .findFirst();
        if (beer.isEmpty()) {
            throw new EntityNotFoundException((
                    String.format("Beer with ID %d not found!", beerId)));
        }
        user.removeDrankBeer(beer.get());
        userRepository.update(user);
    }

    @Override
    public List<Beer> getTop3MostRankedUserBeers(long userId) {
        return userRepository.top3MostRankedUserBeers(userId);
    }

    @Override
    public boolean isAlreadyWished(long beerId, long userId) {
        User user = getUserById(userId);
        Optional<Beer> beer = user.getUsersWishList()
                .stream()
                .filter(b -> b.getId() == beerId)
                .findFirst();
        return beer.isPresent();
    }

    @Override
    public boolean isAlreadyDrank(long beerId, long userId) {
        User user = getUserById(userId);
        Optional<Beer> beer = user.getUsersDrankList()
                .stream()
                .filter(b -> b.getId() == beerId)
                .findFirst();
        return beer.isPresent();
    }

    private void validateUniqueUserEmail(String email) {
        if (userRepository.existByEmail(email)) {
            throw new DuplicateEntityException(
                    String.format("User with email %s already exists!", email));
        }
    }

    private void validateUpdatedUserEmail(String email) {
        if (!userRepository.existByEmail(email)) {
            throw new EntityNotFoundException("You have to enter your registration email.");
        }
    }
}
