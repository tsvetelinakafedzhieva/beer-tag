package com.telerikacademy.team10.beer_tag.services.contracts;

import com.telerikacademy.team10.beer_tag.models.Beer;
import com.telerikacademy.team10.beer_tag.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {

    List<User> getAllUsers();

    User getUserById(long id);

    User getByEmail(String email);

    List<User> filterByEmail(Optional<String> email);

    User create(User user);

    void update(long id, User user);

    void delete(long id);

    Set<Beer> getWishBeers(long id);

    void addBeerToWishList(long userId, long beerId);

    void removeWishBeer(long userId, long beerId);

    Set<Beer> getDrankBeers(long id);

    void addBeerToDrankList(long userId, long beerId);

    void removeDrankBeer(long userId, long beerId);

    List<Beer> getTop3MostRankedUserBeers(long userId);

    boolean isAlreadyWished(long beerId,long userId);

    boolean isAlreadyDrank(long beerId,long userId);

}
