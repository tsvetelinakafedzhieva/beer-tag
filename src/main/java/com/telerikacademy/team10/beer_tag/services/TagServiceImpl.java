package com.telerikacademy.team10.beer_tag.services;

import com.telerikacademy.team10.beer_tag.models.Tag;
import com.telerikacademy.team10.beer_tag.repositories.contracts.TagRepository;
import com.telerikacademy.team10.beer_tag.services.contracts.TagService;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Tag getOrCreate(String name) {
        Optional<Tag> tag = tagRepository.getByName(name);
        return tag.orElseGet(() -> createTag(name));
    }

    private Tag createTag(String name) {
        Tag newTag = new Tag();
        newTag.setName(name);
        tagRepository.create(newTag);
        return newTag;
    }
}
