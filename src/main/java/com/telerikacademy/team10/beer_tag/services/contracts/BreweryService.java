package com.telerikacademy.team10.beer_tag.services.contracts;

import com.telerikacademy.team10.beer_tag.dto.CreateBreweryRequest;
import com.telerikacademy.team10.beer_tag.models.Brewery;

import java.util.List;

public interface BreweryService {

    List<Brewery> getBreweries();

    Brewery getBreweryById(long id);

    Brewery createBrewery(CreateBreweryRequest request, long userId);

    Brewery updateBrewery(long id, CreateBreweryRequest request, long userId);

    void deleteBrewery(long id, long userId);
}
