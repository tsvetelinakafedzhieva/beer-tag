package com.telerikacademy.team10.beer_tag.exceptions;

public class UnauthorizedAccessException  extends RuntimeException {

    public UnauthorizedAccessException(String message) {
        super(message);
    }
}
