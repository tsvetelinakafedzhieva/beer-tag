package com.telerikacademy.team10.beer_tag.configuration;

import com.telerikacademy.team10.beer_tag.models.enums.RoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;

    @Autowired
    public SecurityConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(dataSource);
        return jdbcUserDetailsManager;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(
                        HttpMethod.GET,
                        "/api/beers",
                        "/api/beers/*",
                        "/api/breweries",
                        "/api/breweries/*",
                        "/api/styles",
                        "/api/styles/*"
                ).permitAll()
                .antMatchers(
                        "/files/get/**",
                        "/css/**",
                        "/js/**",
                        "/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/security",
                        "/swagger-ui*",
                        "/webjars/**",
                        "/",
                        "/register",
                        "/register-confirmation",
                        "/beers",
                        "/beers/{beerId:[0-9]+}"
                ).permitAll()
                .antMatchers(
                        "/profile",
                        "/beers/new",
                        "/api/beers",
                        "/api/beers/*"
                ).hasRole(RoleType.ROLE_REGISTERED.name().replace("ROLE_", ""))

                .antMatchers(
                        "/admin",
                        "/users",
                        "/api/breweries",
                        "/api/breweries/*",
                        "/api/styles",
                        "/api/styles/*"

                ).hasRole(RoleType.ROLE_ADMINISTRATOR.name().replace("ROLE_", ""))
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").loginProcessingUrl("/authenticate").defaultSuccessUrl("/beers").permitAll()
                .and()
                .logout().logoutSuccessUrl("/").invalidateHttpSession(true).deleteCookies("JSESSIONID").permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied").defaultAuthenticationEntryPointFor(getRestAuthenticationEntryPoint(), new AntPathRequestMatcher("/api/**"))
                .and().csrf().disable();
    }

    private AuthenticationEntryPoint getRestAuthenticationEntryPoint() {
        return new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
    }
}
